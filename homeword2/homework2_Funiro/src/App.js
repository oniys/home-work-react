import React, {PureComponent} from 'react';
import Header from "./components/Header/Header";
import Carousel from "./components/Сarousel"
import Cart from "./components/Cart/Cart";
import AxiosAPI from './api/AxiosAPI'
import './css/Style.scss'
import Modal from "./components/Modal";


class App extends PureComponent {


    state = {
        data: [],
        isLoading: true,
        showModal: false,
        basket: [],
        preOrder: (push) => {
            this.setState({basket: [...this.state.basket, push]});

            let arr = JSON.parse(localStorage.getItem('basket')) || [];
            arr.push(push);
            localStorage.setItem('basket', JSON.stringify(arr));
        },
        isModal: () => {
            this.setState({showModal: !this.state.showModal})
        },
        favorite: [],
        preFavorite: (push)=>{
            this.setState({favorite: [...this.state.favorite, push]});
            let arr = JSON.parse(localStorage.getItem('favorite')) || [];
            if(!arr.includes(push)){
                arr.push(push);
            }else {
                for( let i = 0; i < arr.length; i++){
                    if ( arr[i] === push) {
                        arr.splice(i, 1);
                        i--;
                    }
                }
            }
            localStorage.setItem('favorite', JSON.stringify(arr));
        }
    };

    async componentDidMount() {
        let data = await AxiosAPI.get('/', {
            params: {
                data: 1,
                inc: 'id,title,price,article,picture,color'
            }
        });
        this.setState({
            data: data.data.data,
            isLoading: false
        });

    }



    renderCart() {
        return (
            <Cart
                data={this.state.data}
                isModal={this.state.isModal}
                showModal={this.state.showModal}
                preOrder={this.state.preOrder}
                preFavorite={this.state.preFavorite}
            />
        );
    }

    render() {
        return (
            <div>
                <div className="container-top">
                    <Header basket={this.state.basket} favorite={this.state.favorite}/>
                    <div>
                        <Carousel/>
                    </div>
                </div>
                {this.state.isLoading ? <div>Loading...</div> : this.renderCart()}
                <Modal
                    data={this.state.data}
                    basket={this.state.basket}
                    preOrder={this.state.preOrder}
                    showModal={this.state.showModal}
                    isModal={this.state.isModal}
                />
            </div>
        );
    }
}

export default App;