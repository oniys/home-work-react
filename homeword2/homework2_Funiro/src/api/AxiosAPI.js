import axios from 'axios';

const _URL = 'localhost';
const _PORT =  '8033';

export default axios.create({
    baseURL: `http://${_URL}:${_PORT}/api`,
    responseType: "json"
},[]);
