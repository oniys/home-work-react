import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    render(props) {
        return (
            <div>
                <button className="btn-bra" onClick={() => {
                        if(this.props.onClick.name === "isModal"){
                            if (!!this.props.data) this.props.data.preOrder(this.props.data.id)
                        }
                            this.props.onClick(this.props.data.id)
                    }}>
                    {this.props.icon}
                    {this.props.textBtn}
                </button>
            </div>
        );
    }
}

export default Button