import React, {PureComponent} from 'react';
import CartElem from "./CartElem";
import PropTypes from 'prop-types';
import "./Cart.scss"

function Cart(props) {

    function renderCartElem(key) {

        return (
            <CartElem
                key={key.id}
                title={key.title}
                price={key.price}
                article={key.article}
                id={key.id}
                picture={key.picture}
                color={key.color}
                modal={props.isModal}
                showModal={props.showModal}
                preOrder={props.preOrder}
                preFavorite={props.preFavorite}
            />
        );
    }

    return (
        <div key={props.id}>
            <ul className="container-cart">
                {
                    props.data.map(renderCartElem)
                }
            </ul>
            <button>Show More</button>
        </div>
    );
}

Cart.propTypes = {
    data: PropTypes.array.isRequired,
    showModal: PropTypes.bool,
    isModal: PropTypes.func,
    preOrder: PropTypes.func,
    preFavorite: PropTypes.func
};

export default Cart;