import React from 'react';
import SvgBasket from "../SvgBasket";
import Button from "../Button/Button";
import SvgFavorit from "../SvgFavorit";
import PropTypes from 'prop-types'

function CartElem(props) {
    return (
        <li className="cart-elem">
            <img src={props.picture} alt={props.title} className="card-elem-picture"></img>
            <div style={{display: "flex"}}>
                <Button onClick={props.modal} textBtn="Add to cart" icon={<SvgBasket/>} data={props}/>
                <Button className="btn"  onClick={props.preFavorite} icon={<SvgFavorit col={

                    JSON.parse(
                        localStorage.getItem('favorite') ?
                            localStorage.getItem('favorite').includes(props.id)
                            :
                            null
                    ) ? 'red' : null

                }/>} data={props}/>
            </div>

            <div className="card-elem-info">
                <h3 className="card-elem-title">{props.title}</h3>
                <small className="card-elem-article">{props.article}</small>
                <p className="card-elem-price" style={{color: props.color}}>{props.price} USD</p>
            </div>
        </li>
    );
}

CartElem.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.string,
    id: PropTypes.string.isRequired,
    picture: PropTypes.string,
    color: PropTypes.string,
    modal: PropTypes.func,
    preFavorite: PropTypes.func.isRequired,
    preOrder: PropTypes.func.isRequired
}

export default CartElem;