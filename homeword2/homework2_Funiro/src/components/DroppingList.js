import React, {Component} from 'react';
import '../css/DroppingList.scss'

class DroppingList extends Component {
    render() {
        return (
            <div>
                <ul className="dropping-list">
                    <li className="dropping-item">Products</li>
                    <li className="dropping-item">Rooms</li>
                    <li className="__item">Inspirations</li>
                </ul>
            </div>
        );
    }
}

export default DroppingList;