import React, {PureComponent} from 'react';
import DroppingList from '../DroppingList'
import Inputs from "../Inputs";
import './Header.scss';
import SvgBasket from "../SvgBasket";
import SvgFavorit from "../SvgFavorit";
import PropTypes from 'prop-types';

class Header extends PureComponent {
    render(props) {
        const styleInputs = {
            width: `${473}px`,
            fontSize: `${14}px`,
            color: "#616161",
            paddingLeft: "50px",
            svg: <svg className="search-icon" width="20" height="20" viewBox="0 0 20 20" fill="none"
                      xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M16.6667 16.6666L12.5802 12.5801M12.5802 12.5801C13.5604 11.5999 14.1667 10.2457 14.1667 8.74992C14.1667 5.75838 11.7415 3.33325 8.75 3.33325C5.75846 3.33325 3.33334 5.75838 3.33334 8.74992C3.33334 11.7415 5.75846 14.1666 8.75 14.1666C10.2458 14.1666 11.5999 13.5603 12.5802 12.5801Z"
                    stroke="#333333" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        }

        return (
            <div className="container-header">
                <span><h1 className="logo-company">Funiro</h1></span>
                <DroppingList/>
                <Inputs styleSearch={styleInputs}/>
                <div className="all-container-icon">
                    <div className="icons-fun">
                        <SvgFavorit/>
                        <span className="counter-icon">{
                            JSON.parse(localStorage.getItem('favorite'))
                                ? JSON.parse(localStorage.getItem('favorite')).length
                                : ''
                        }</span>
                    </div>
                    <div className="icons-fun">
                        <SvgBasket/>
                        <span className="counter-icon">{
                            JSON.parse(localStorage.getItem('basket'))
                                ? JSON.parse(localStorage.getItem('basket')).length
                                : ''
                        }</span>
                    </div>
                </div>
            </div>
        );
    }
}

Header.propType = {
    basket: PropTypes.array,
    favorite: PropTypes.string
}

export default Header;