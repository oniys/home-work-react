import React, {Component} from 'react';
import '../css/Modal-style.scss'
import PropTypes from 'prop-types';

class Modal extends Component {
    render(props) {

        const modals = this.props.showModal ?
            (<div className="overlay">
                <div className="window-modal">
                    <div className="close" onClick={() => {
                        this.props.isModal()
                    }}>╳
                    </div>
                    <div> Congratulations, the item has been added to your cart.!
                    </div>
                    <button className="close" onClick={() => {
                        this.props.isModal()
                    }}>Ok
                    </button>
                </div>
            </div>) : null
        return modals
    }
}

Modal.propsType = {
    data: PropTypes.array,
    basket: PropTypes.array,
    showModal: PropTypes.bool,
    preOrder: PropTypes.func,
    isModal: PropTypes.func
}

export default Modal;