import React, { Component } from 'react';
import Slider from 'react-slick';
import '../css/SlickCarousel/slick-theme.css';
import '../css/SlickCarousel/slick.css';
import '../css/Carousel.scss'

//
import Azhouss from '../image/Azhouss.jpeg'
import Bohauss from '../image/Bohauss.png'
import Fahouss from '../image/Fahouss.jpg'


const cssstyle = `
.container_ {
    overflow: hidden;
  padding: 0px 0px 40px 0px;
}
.slick-next:before, .slick-prev:before {
    color: #000;
}
.slick-slider{
width: 120%
}
.slick-arrow{
position: relative;
}


`


export default class Carousel extends Component {

    render() {
        const settings = {
            className: "center",
            centerMode: true,
            infinite: true,
            centerPadding: "375px",
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 500
        };
        return (
            <div className="container_">
                <style>{cssstyle}</style>
                <Slider {...settings}>
                    <div>
                        <img className="slider-img" src={Azhouss} alt=""/>
                    </div>
                    <div>
                        <img className="slider-img" src={Bohauss} alt=""/>
                    </div>
                    <div>
                        <img className="slider-img" src={Fahouss} alt=""/>
                    </div>
                </Slider>
            </div>
        );
    }
}

