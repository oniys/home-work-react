let express = require('express');
let cors = require('cors');
let app = express();
app.use(cors());

const port = process.env.PORT || 8033;

const data =
    {
        "data": [
            {   "id": "1",
                "title": "Syltherine",
                "price": "2.500",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/efa6bf755350c6fd270b2c8379646406.png",
                "color": "red"
            },
            {   "id": "2",
                "title": "Leviosa",
                "price": "2.500",
                "article": "Fittings",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/1056c915ab9b5c2956773776d8552f21.png",
                "color": "black"
            },
            {   "id": "3",
                "title": "Lolito",
                "price": "7.000",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/8bc73333f24c58b69d189642a9382da6.png",
                "color": "black"
            },
            {   "id": "4",
                "title": "Respira",
                "price": "500",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/e6f4c310219eefed91aba1fe0f5a184c.png",
                "color": "black"
            },
            {   "id": "5",
                "title": "Grifo",
                "price": "1.500",
                "article": "Fittings",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/23d933552233702997fc68f2960e6e9c.png",
                "color": "red"
            },
            {   "id": "6",
                "title": "Muggo",
                "price": "150",
                "article": "Fittings",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/6560327e826183c5cf9d36d2235979b7.png",
                "color": "black"
            },
            {   "id": "7",
                "title": "Pingky",
                "price": "7.000",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/42ed435c1925fd28e49df7a1637c2a03.png",
                "color": "red"
            },
            {   "id": "8",
                "title": "Potty",
                "price": "500",
                "article": "Fittings",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/225b6cb09f4ae7dc628327a17c1458e9.png",
                "color": "black"
            },
            {   "id": "9",
                "title": "Defila",
                "price": "50000",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/a4fc9c1f516f1d134a4e2b6c1691eff5.jpg",
                "color": "black"
            },
            {   "id": "10",
                "title": "Flopsi",
                "price": "3000",
                "article": "Design",
                "picture": "https://s8.hostingkartinok.com/uploads/images/2021/03/e3ce41cf801fe3c208eb8ca5ee4b3a75.jpeg",
                "color": "red"
            }
        ],
    };

app.get('/api/', (req, res) => {
  res.send(data)
})

app.listen(port, () => {
  console.log(`server listeting on port ${port}`)
})
